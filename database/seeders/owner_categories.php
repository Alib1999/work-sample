<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class owner_categories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $instance= new \App\Models\owner\owner_categories();
        $instance->key =1;
        $instance->title = 'restaurant';
        $instance->save();

        $instance= new \App\Models\owner\owner_categories();
        $instance->key =2;
        $instance->title = 'fastfood';
        $instance->save();


        $instance= new \App\Models\owner\owner_categories();
        $instance->key =3;
        $instance->title = 'coffee';
        $instance->save();

        $instance= new \App\Models\owner\owner_categories();
        $instance->key =4;
        $instance->title = 'cookie';
        $instance->save();

        $instance= new \App\Models\owner\owner_categories();
        $instance->key =5;
        $instance->title = 'ice cream';
        $instance->save();

    }
}
