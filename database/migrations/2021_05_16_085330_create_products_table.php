<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_title');
            $table->foreign('category_title')->references('id')->on('owner_categories')->onDelete('cascade');
            $table->string('name');
            $table->unsignedBigInteger('owner_location_id');
            $table->foreign('owner_location_id')->references('id')->on('owners')->onDelete('cascade');
            $table->string('description');
            $table->integer('price');
            $table->unsignedBigInteger('code');
            $table->string('logo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
