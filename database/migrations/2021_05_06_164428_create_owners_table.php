<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('owner_categories')->onDelete('cascade');
            $table->string('tel');
            $table->string('mobile');
            $table->enum('status',['0','1','2','3'])->default('1');
            $table->string('description');
            $table->string('address');
            $table->string('password');
            $table->string('logo')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
