<?php

use App\Models\Owner\Owners;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::middleware('ownerAuth')->group(function (){
    Route::post('/owner/profile', function (Request $request) {
dd($request->all());

    })->name('apiShowProfile');
});


Route::post('/owner/auth',function (Request $request){

    $mobile = $request->all('mobile');
    $owner = Owners::where('mobile',$mobile)->first();
    if (!$owner){

        return response()->json([
            'status'=>'404',
            'data'=>[],
            'message'=>'owner not found',
        ]);
    }

    $key = config('JWT.secret_key');
    $payload = array(
        "exp" => time() + config('JWT.ttl'),
        "jti" =>$mobile,

    );

    $jwt = JWT::encode($payload, $key,);

//    return redirect()->route('postProfile',['jwt'=>$jwt]);

})->name('apiProfile');
