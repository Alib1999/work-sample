<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');


//-------------------------owners--------------------------------

Route::prefix('owner')->group(function (){

    Route::get('/', [\App\Http\Controllers\Owner\OwnersController::class,'index'])->name('owners');

    Route::get('/owner/profile', [\App\Http\Controllers\Owner\OwnersController::class,'getProfile'])->name('profile');


    Route::get('/create', [\App\Http\Controllers\Owner\OwnersController::class,'createOwner'])->name('owner.create');
    Route::post('/store', [\App\Http\Controllers\Owner\OwnersController::class,'storeOwner'])->name('owner.store');

    Route::get('/{id}/edit', [\App\Http\Controllers\Owner\OwnersController::class,'editOwner'])->name('owner.edit');
    Route::post('/{id}/update', [\App\Http\Controllers\Owner\OwnersController::class,'updateOwner'])->name('owner.update');

    Route::delete('/{id}/delete', [\App\Http\Controllers\Owner\OwnersController::class,'deleteOwner'])->name('owner.delete');


});

//-------------------------user--------------------------------

Route::get('/users', [\App\Http\Controllers\UserController::class,'index'])->name('users');
Route::get('/users/create', [\App\Http\Controllers\UserController::class,'createUsers'])->name('user.create');


Route::post('file-upload',[\App\Http\Controllers\UserController::class,'userProfile'])->name('user.profile');


//-------------------------products--------------------------------
Route::prefix('products')->group(function (){

Route::get('/',[\App\Http\Controllers\products\Products::class,'products'])->name('products');
Route::get('/create/products',[\App\Http\Controllers\products\Products::class,'createProducts'])->name('create.products');
Route::post('/store/products',[\App\Http\Controllers\products\Products::class,'storeProducts'])->name('store.products');

Route::get('/fastfood',[\App\Http\Controllers\products\Products::class,'fastfood'])->name('fastfood');

});


