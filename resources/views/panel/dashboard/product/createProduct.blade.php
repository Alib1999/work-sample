@extends('panel.layout.master')
@section('main')


    <div class="col-md-11 ml-lg-4">
        <div class="card">
            <div class="card-header">
                <h3 class="title">Add product</h3>
            </div>
            <div class="card-body">
                <form method="post" autocomplete="off" action="{{route('store.products')}}"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-5">
                            <label>category</label>
                            <select class="form-control" name="category_title">
                                <option class="form-group" value=" ">select your category</option>
                                @if($product_categories)
                                    @foreach($product_categories as $product_category)
                                        <option class="form-check" name="category_title"
                                                value="{{$product_category->title}}"
                                        @if(old('category_title'))
                                            {{(old('category_title') ==$product_category->title ? 'selected' : '')}}
                                            @endif>
                                            {{$product_category->title}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            @error('category_title')
                            <div class="alert alert-danger">
                                <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                </button>
                                <span>{{$errors->first('category_title')}}</span>
                            </div>
                            @enderror
                        </div>

                        <div class="col-md-5">
                            <label>location</label>
                            <select class="form-control" name="owner_location_id">
                                <option class="form-group" value=" ">select your location</option>
                                @if($owners)
                                    @foreach($owners as $owner)
                                        <option class="form-check" name="owner_location_id"
                                                value="{{$owner->name}}"
                                        @if(old('owner_location_id'))
                                            {{(old('owner_location_id') ==$owner->name ? 'selected' : '')}}
                                            @endif>
                                            {{$owner->name}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                            @error('location')
                            <div class="alert alert-danger">
                                <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                        aria-label="Close">
                                </button>
                                <span><b></b>{{$errors->first('location')}}</span>
                            </div>
                            @enderror
                        </div>

                        <div class="col-lg-5">
                            <div class="form-group">
                                <label>name</label>
                                <input type="text" class="form-control "
                                       name="name" value="{{old('name')}}">

                                @error('name')
                                <div class="alert alert-danger">
                                    <button type="button" aria-hidden="true" class="close"
                                            data-dismiss="alert"
                                            aria-label="Close">
                                    </button>
                                    <span><b></b>{{$errors->first('name')}}</span>
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-5">
                            <div class="form-group">
                                <label>code</label>
                                <input type="text" class="form-control "
                                       name="code" value="{{old('code')}}">

                                @error('code')
                                <div class="alert alert-danger">
                                    <button type="button" aria-hidden="true" class="close"
                                            data-dismiss="alert"
                                            aria-label="Close">
                                    </button>
                                    <span><b></b>{{$errors->first('code')}}</span>
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-5">
                            <div class="form-group">
                                <label>price</label>
                                <input type="text" class="form-control "
                                       name="price" value="{{old('price')}}">

                                @error('price')
                                <div class="alert alert-danger">
                                    <button type="button" aria-hidden="true" class="close"
                                            data-dismiss="alert"
                                            aria-label="Close">
                                    </button>
                                    <span><b></b>{{$errors->first('price')}}</span>
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-10">
                            <div class="form-group">
                                <label>description</label>
                                <input type="text" class="form-control" name="description"
                                       value="{{old('description')}}">
                                @error('description')
                                <div class="alert alert-danger">
                                    <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                        <i class="tim-icons icon-simple-remove"></i>
                                    </button>
                                    <span><b></b>{{$errors->first('description')}}</span>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <div class="col-md-2 ">
                            <div class="form-group">Logo

                                <div class="form-group">
                                    <input type="file" id="exampleInputFile" name="logo">
                                    <p class="btn btn-dark">choose logo</p>

                                    @error('logo')
                                    <div class="alert alert-danger">
                                        <span>{{$errors->first('logo')}}</span>
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
