@extends('panel.layout.master')
@section('main')

    <div class="main-content ">
        <div class="text-center">
            <div class="card-columns col-xl-12">
                <div class="col-md-10 ml-lg-auto">
                    <div class="card card-user">
                        <div class="card-body">
                            <p class="card-text">

                            <div class="author">
                                <div class="block block-one"></div>
                                <div class="block block-two"></div>
                                <div class="block block-three"></div>
                                <div class="block block-four"></div>
                                <a href="{{route('fastfood')}}">
                                    <img class="photo" src="{{asset('image/fastfood.jpg')}}">
                                    <h5 class="title">FAST FOOD</h5>
                                </a>
                                <p class="description">
                                    Ceo/Co-Founder
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-10 ml-lg-n2">
                    <div class="card card-user">
                        <div class="card-body">
                            <p class="card-text">

                            <div class="author">
                                <div class="block block-one"></div>
                                <div class="block block-two"></div>
                                <div class="block block-three"></div>
                                <div class="block block-four"></div>
                                <a href="javascript:void(0)">
                                    <img class="photo" src="{{asset('image/aa.jpg')}}">
                                    <h5 class="title">COOKIE</h5>
                                </a>
                                <p class="description">
                                    Ceo/Co-Founder
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-10 ml-lg-nlg">
                    <div class="card card-user">
                        <div class="card-body">
                            <p class="card-text">

                            <div class="author">
                                <div class="block block-one"></div>
                                <div class="block block-two"></div>
                                <div class="block block-three"></div>
                                <div class="block block-four"></div>
                                <a href="javascript:void(0)">
                                    <img class="photo" src="{{asset('image/ice.jpg')}}">
                                    <h5 class="title">ICE CREAM</h5>
                                </a>
                                <p class="description">
                                    Ceo/Co-Founder
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center col-xl-12 ml-lg-n5">
            <a class="btn btn-primary col-md-5" href="{{route('create.products')}}">new products</a>
        </div>

    </div>

@endsection
