@extends('panel.layout.master')
@section('main')

    <div class="content">
        @csrf
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <h3 class="title">Create Owner</h3>
                    </div>
                    <div class="card-body">
                        <form method="post" autocomplete="off" action="{{route('owner.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>name</label>
                                        <input type="text" class="form-control"
                                               placeholder="Company" name="name" value="{{old('name')}}">

                                        @error('name')
                                        <div class="alert alert-danger">
                                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                <i class="tim-icons icon-simple-remove"></i>
                                            </button>
                                            <span><b></b>{{$errors->first('name')}}</span>
                                        </div>
                                        @enderror

                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>email</label>
                                        <input type="text" class="form-control" name="email" value="{{old('email')}}">
                                        @error('email')
                                        <div class="alert alert-danger">
                                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                <i class="tim-icons icon-simple-remove"></i>
                                            </button>
                                            <span><b></b>{{$errors->first('email')}}</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <label>category</label>
                                    <select class="form-control" name="category_id">
                                        <option class="form-group" value=" ">select your category</option>
                                        @if($owner_categories)
                                            @foreach($owner_categories as $owner_category)
                                                <option class="form-group" name="category_id"
                                                        value="{{$owner_category->id}}"
                                                @if(old('owner_category'))
                                                    {{(old('owner_category') ==$owner_category->id ? 'selected' : '')}}
                                                    @endif>
                                                    {{$owner_category->title}}
                                                </option>
                                            @endforeach
                                        @endif

                                    </select>

                                    @error('category_id')
                                    <div class="alert alert-danger">
                                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                                aria-label="Close">
                                            <i class="tim-icons icon-simple-remove"></i>
                                        </button>
                                        <span><b></b>{{$errors->first('category_id')}}</span>
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>tel</label>
                                        <input type="text" class="form-control" name="tel" value="{{old('tel')}}">
                                        @error('tel')
                                        <div class="alert alert-danger">
                                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                <i class="tim-icons icon-simple-remove"></i>
                                            </button>
                                            <span><b></b>{{$errors->first('tel')}}</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>mobile</label>
                                        <input type="text" class="form-control" name="mobile" value="{{old('mobile')}}">
                                        @error('mobile')
                                        <div class="alert alert-danger">
                                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                <i class="tim-icons icon-simple-remove"></i>
                                            </button>
                                            <span><b></b>{{$errors->first('mobile')}}</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>status</label>

                                        <div class="form-group">
                                            <div class="custom-radio">
                                                <label>
                                                    <input type="radio" name="status" id="optionsRadios1"
                                                           value="{{\App\Models\Owner\Owners::$status['activate']}}"

                                                           @if(old('status'))
                                                           {{old('status') == \App\Models\Owner\Owners::$status['activate'] ? 'checked' : ''}}
                                                           @else
                                                           checked>
                                                    @endif
                                                    activate
                                                </label>
                                            </div>
                                            <div class="custom-radio">
                                                <label>
                                                    <input type="radio" name="status" id="optionsRadios2"
                                                           value="{{\App\Models\Owner\Owners::$status['deactivate']}}"
                                                        {{old('status') == \App\Models\Owner\Owners::$status['deactivate'] ? 'checked' : ''}}
                                                    >
                                                    deactivate
                                                </label>
                                            </div>
                                            <div class="custom-radio">
                                                <label>
                                                    <input type="radio" name="status" id="optionsRadios3"
                                                           value="{{\App\Models\Owner\Owners::$status['soon']}}"
                                                        {{old('status') == \App\Models\Owner\Owners::$status['soon'] ? 'checked' : ''}}
                                                    >
                                                    soon
                                                </label>
                                            </div>
                                            <div class="custom-radio">
                                                <label>
                                                    <input type="radio" name="status" id="optionsRadios4"
                                                           value="{{\App\Models\Owner\Owners::$status['block']}}"
                                                        {{old('status') == \App\Models\Owner\Owners::$status['block'] ? 'checked' : ''}}
                                                    >
                                                    block
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label>description</label>
                                        <input type="text" class="form-control" name="description"
                                               value="{{old('description')}}">
                                        @error('description')
                                        <div class="alert alert-danger">
                                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                <i class="tim-icons icon-simple-remove"></i>
                                            </button>
                                            <span><b></b>{{$errors->first('description')}}</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group">
                                        <label>address</label>
                                        <input type="text" class="form-control" name="address"
                                               value="{{old('address')}}">
                                        @error('address')
                                        <div class="alert alert-danger">
                                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                <i class="tim-icons icon-simple-remove"></i>
                                            </button>
                                            <span><b></b>{{$errors->first('address')}}</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>password</label>
                                        <input type="password" class="form-control" name="password">
                                        @error('password')
                                        <div class="alert alert-danger">
                                            <button type="button" aria-hidden="true" class="close" data-dismiss="alert"
                                                    aria-label="Close">
                                                <i class="tim-icons icon-simple-remove"></i>
                                            </button>
                                            <span><b></b>{{$errors->first('password')}}</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label>password confirmation</label>
                                        <input type="password" class="form-control" name="password_confirmation">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="form-group">Logo

                                            <div class="form-group">
                                                <input type="file" id="exampleInputFile" name="logo">
                                                @error('logo')
                                                <div class="alert alert-danger">
                                                    <span>{{$errors->first('logo')}}</span>
                                                </div>
                                                @enderror
                                                <p class="btn">choose logo</p>
                                            </div>
                                        </div>
                                        @error('logo')
                                        <div class="alert alert-danger">
                                            <span>{{$errors->first('logo')}}</span>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-fill btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
