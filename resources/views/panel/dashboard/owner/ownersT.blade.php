@extends('panel.layout.master')
@section('main')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card  card-plain">
                    <div class="card-header">
                        <h4 class="card-title"> Table on Owners</h4>
                        <p class="category"> Here is a subtitle for this table</p>
                    </div>
                    <div>
                        <a href="{{route('owner.create')}}" class="btn btn-warning">New Owner</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table tablesorter" id="">
                                <thead class="text-primary">
                                <tr>
                                    <th>
                                        ROW
                                    </th>
                                    <th>
                                        name
                                    </th>
                                    <th>
                                        email
                                    </th>
                                    <th>
                                        mobile
                                    </th>
                                    <th>
                                        status
                                    </th>
                                    <th>
                                        address
                                    </th>
                                    <th>
                                        logo
                                    </th>

                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($owners) > 0)
                                    @foreach($owners as $owner)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$owner->name}}</td>
                                            <td>{{$owner->email}}</td>
                                            <td>{{$owner->mobile}}</td>
                                            <td>{{$owner->status}}</td>
                                            <td>{{$owner->address}}</td>
                                            <td>
                                                    <div class="fa-photo col-xl-4">
                                                        <img src="{{asset("uploads/$owner->logo")}}" alt="profile">
                                                    </div>
                                            </td>
                                            <td>

                                                <form action="{{ route('owner.delete', $owner->id)}}" method="post">

                                                    <a href="{{route('owner.edit',['id'=>$owner->id])}}"
                                                       class="btn btn-primary">Edit</a>

                                                    <input class="btn btn-danger" type="submit" value="delete">
                                                    @method('DELETE')
                                                    @csrf
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
