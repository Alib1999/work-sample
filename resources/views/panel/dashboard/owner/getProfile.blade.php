@extends('panel.layout.master')
@section('main')
    <div class="col-xl-8 col-lg-1 col-md-8 col-sm-10 mx-auto text-center form p-8">
        <div class="card">
            <hr>
            <h4 class="text-center">set profile</h4>
            <hr>
            <div class="col-sm-offset-3 mx-auto text-center form p-8">
                <form method="post" action="{{route('user.profile')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">Logo

                        <div class="form-group">
                            <input type="file" id="exampleInputFile" name="logo">
                            @error('logo')
                            <div class="alert alert-danger">
                                <span>{{$errors->first('logo')}}</span>
                            </div>
                            @enderror
                            <p class="btn">choose logo</p>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning">Go</button>
                </form>
            </div>
        </div>
    </div>

@endsection
