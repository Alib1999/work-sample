@extends('panel.layout.master')
@section('main')

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card  card-plain">
                    <div class="card-header">
                        <h4 class="card-title"> Table on Users</h4>
                        <p class="category"> Here is a subtitle for this table</p>
                    </div>
                    <div>
                        <div class="btn btn-warning">
                            <a class="dropdown-backdrop" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <h6>NEW USER</h6>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="col-md-12">
                                @csrf
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table tablesorter" id="">
                                <thead class="text-primary">
                                <tr>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        email
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($users) > 0)
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                <a href="#" class="btn btn-primary">Edit</a>
                                                <a href="#" class="btn btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
