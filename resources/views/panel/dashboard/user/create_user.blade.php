@extends('panel.layout.master')
@section('main')

    <div class="btn btn-warning">
        <a class="dropdown-item" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="col-md-12">
            @csrf
        </form>
    </div>

@endsection
