<aside class="main-sidebar">

    <div class="sidebar">

        {{--        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red"--}}

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="javascript:void(0)" class="simple-text logo-mini">
                    CT
                </a>
                <a href="javascript:void(0)" class="simple-text logo-normal">
                    Creative Tim
                </a>
            </div>
            <ul class="nav">
                <li class="active ">
                    <a href="{{route('dashboard')}}">
                        <i class="tim-icons icon-chart-pie-36"></i>
                        <h5>Dashboard</h5>
                    </a>
                </li>
                <li>
                    <a href="{{route('users')}}">
                        <i class="tim-icons icon-atom"></i>
                        <h5>Users</h5>
                    </a>
                </li>

                <li>
                    <a href="{{route('owners')}}">
                        <i class="tim-icons icon-atom"></i>
                        <h5>OWNER</h5>
                    </a>
                </li>

                <li>
                    <a href="{{route('profile')}}">
                        <i class="tim-icons icon-atom"></i>
                        <h5>PROFILE</h5>
                    </a>
                </li>
                <li>
                    <a href="{{route('products')}}">
                        <i class="tim-icons icon-atom"></i>
                        <h5>PRODUCTS</h5>
                    </a>
                </li>

            </ul>
        </div>
    </div>
</aside>

