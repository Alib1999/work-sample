<?php

namespace App\Models\Owner;

use Illuminate\Database\Eloquent\Model;

class Owners extends Model
{

    protected $fillable = [
        'name', 'email', 'category_id',
        'tel', 'mobile', 'status',
        'description', 'address', 'password',
        'logo',
    ];

    public static $status = [
            'deactivate' => '0',
            'activate'   => '1',
            'soon'       => '2',
            'block'      => '3',
        ];
}

