<?php

namespace App\Models\Owner;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class owner_categories extends Model
{
    protected $fillable=[
        'title','key'
    ];
}
