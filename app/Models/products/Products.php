<?php

namespace App\Models\products;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'category_title',
        'name',
        'description',
        'price',
        'code',
        'logo',
        'owner_location_id'
    ];
}
