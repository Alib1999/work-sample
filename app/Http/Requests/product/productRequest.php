<?php

namespace App\Http\Requests\product;

use Illuminate\Foundation\Http\FormRequest;

class productRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:35',
            'category_title' => 'required|exists:owner_categories,title',
            'owner_location_id' => 'required|exists:owners,name',
            'description' => 'required',
            'price' => 'required|integer',
            'code' => 'required|integer|unique:products',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,',

        ];
    }
}
