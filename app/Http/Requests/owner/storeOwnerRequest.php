<?php

namespace App\Http\Requests\owner;

use App\Models\Owner\Owners;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class storeOwnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'        => 'required|min:3|max:25',
            'email'       => 'required|email|',
            'category_id' => 'required|numeric|exists:owner_categories,id',
            'tel'         => 'required|numeric',
            'mobile'      => 'required|numeric',
            'status'      => [
                'required',
                Rule::in(Owners::$status)
                ],
            'description' => 'required',
            'address'     => 'required|min:8',
            'password'    => 'required|confirmed|min:6|max:8|unique:owners',
            'logo'        => 'nullable',
        ];

    }
    public function messages()
    {
        return [
            'category_id.required' => 'The category field is required.',

        ];
    }
}
