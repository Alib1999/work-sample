<?php

namespace App\Http\Controllers;

use App\Http\Requests\owner\ownerProfileRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{
    public function index()
    {
    $users = User::all();
    return \view('panel.dashboard.user.usersT',compact('users'));

    }

    public function createUsers()
    {
        return \view('panel.dashboard.user.create_user');

    }

    public static function userProfile(ownerProfileRequest $request)
    {
        $fileName = time().'.'.$request->file('logo')->extension();

        $request->file('logo')->move(public_path('uploads'),$fileName);

        return \view('',compact($fileName));
    }




}
