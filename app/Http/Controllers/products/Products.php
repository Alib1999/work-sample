<?php

namespace App\Http\Controllers\products;

use App\Http\Controllers\Controller;
use App\Http\Requests\product\productRequest;
use App\Models\Owner\owner_categories;
use App\Models\Owner\Owners;
use Illuminate\Http\Request;

class Products extends Controller
{
    public function products()
    {
        return view('panel.dashboard.product.productsCategory');

    }

    public function fastfood()
    {
        return view('site.fastfood.fastfood');

    }

    public function createProducts()
    {
        $owners = Owners::all();
        $product_categories = owner_categories::all();
        return view('panel.dashboard.product.createProduct', compact('product_categories', 'owners'));

    }

    public function storeProducts(Request $request)
    {
        try {
            $product_logo =time().".".$request->file('logo')->extension();
            $request->file('logo')->move(public_path('uploads/product_image'),$product_logo);

            $data = [
              'category_title'=>$request->category_title ,
              'name'            =>$request->name ,
              'description'=>$request->description ,
              'price'=>$request->price ,
              'code'=>$request->code ,
              'logo'=>$request->logo ,
              'owner_location_id'=>$request->owner_location_id ,
            ];
            dd($data,$request->all());

        } catch (\Exception $exception) {
            dd($exception);
        }
    }
}
