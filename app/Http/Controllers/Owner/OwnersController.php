<?php

namespace App\Http\Controllers\Owner;

use App\Http\Controllers\Controller;
use App\Http\Requests\owner\storeOwnerRequest;
use App\Http\Requests\owner\updateOwnerRequest;
use App\Models\Owner\owner_categories;
use App\Models\Owner\Owners;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function view;

class OwnersController extends Controller
{
    public function index()
    {
        $owners = Owners::all();

        return view('panel.dashboard.owner.ownersT', compact('owners'));


    }

    public function createOwner()
    {
        $owner_categories = owner_categories::all();
        return view('panel.dashboard.owner.create_owner', compact('owner_categories'));

    }

    public function storeOwner(Request $request)
    {

        try {
            DB::beginTransaction();
            if ($request->file('logo')) {

                $logo = time() . "." . $request->file('logo')->extension();
                $request->file('logo')->move(public_path('uploads'), $logo);

                $data = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'category_id' => $request->category_id,
                    'tel' => $request->tel,
                    'mobile' => $request->mobile,
                    'status' => $request->status,
                    'description' => $request->description,
                    'address' => $request->address,
                    'password' => $request->password,
                    'logo' => $logo,
                ];

            } else {
                $data = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'category_id' => $request->category_id,
                    'tel' => $request->tel,
                    'mobile' => $request->mobile,
                    'status' => $request->status,
                    'description' => $request->description,
                    'address' => $request->address,
                    'password' => $request->password,
                    'logo' => null,
                ];
            }


            (new Owners())->create($data);

            DB::commit();
            return redirect()->route('owners');


        } catch (Exception $exception) {
            dd($exception,$data);
            DB::rollBack();
        }


    }


    public function editOwner($id)
    {
        $owner = Owners::where('id', $id)->first();

        $owner_categories = owner_categories::all();

        return view('panel.dashboard.owner.edit_owner', compact('owner', 'owner_categories'));


    }

    public function updateOwner(updateOwnerRequest $request, $id)
    {
        try {
            $owner = Owners::where('id', $id)->first();

            DB::beginTransaction();


            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'category_id' => $request->category_id,
                'tel' => $request->tel,
                'mobile' => $request->mobile,
                'status' => $request->status,
                'description' => $request->description,
                'address' => $request->address,
                'password' => $request->password,
                'logo' => $request->logo,
            ];
            if ($request->has('password') and $request->input('password') != '')
                $data = array_merge($data, [
                    'password' => $request->password,

                ]);

            $owner->update($data);
            DB::commit();

            return redirect()->route('owners');

        } catch (Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors($exception)->withInput();
        }


    }

    public function deleteOwner(Request $request, $id, Exception $exception)
    {
        try {
            $owner = Owners::where('id', $id)->first();

            if (!$owner)
                return back()->withErrors($exception)->withInput();

            DB::beginTransaction();
            $owner->delete();
            DB::commit();


        } catch (Exception $exception) {
            DB::rollBack();
            dd($exception);
        }
        return redirect()->route('owners')->withInput([
            'success'
        ]);


    }


}
