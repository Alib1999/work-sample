<?php

namespace App\Http\Middleware;

use App\Models\Owner\Owners;
use Closure;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;

class OwnerAuthentication
{
    private static function getOwner($jti)
    {
        $mobile = $jti->mobile;
        $owner = Owners::where('mobile',$mobile)->first();
        if ($owner){
            app()->instance(Owners::class, $owner);
            return true;
        }
        else
            return false;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            var_dump($request->all(),$request->header());
            $secret_key = config('JWT.secret_key');
            $token = $request->header('authentication');

            $decoded = JWT::decode($token, $secret_key, array('HS256'));

            $result = self::getOwner($decoded->jti);
            if ($result){
                return $next($request);

            }
            else
                return response()->json([
                    'status' => '404',
                    'data' => [],
                    'message' => 'result false',

                ]);




        } catch (\Exception $exception) {
            dd($exception);
        };

    }
}
